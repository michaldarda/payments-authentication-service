defmodule AuthenticationService.SessionController do
  use AuthenticationService.Web, :controller

  import Ecto.Query

  def create(conn, %{"user" => user_params}) do
    query = from u in AuthenticationService.User,
    where: u.login == ^user_params["login"]

    user = Repo.one(query)

    pass_match = Comeonin.Bcrypt.checkpw(user_params["password"], user.crypted_password)

    if pass_match do
      render conn, session: user
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render(AuthenticationService.ChangesetErrorView, "error.json", changeset: %{"error" => "password or login is incorrect"})
    end
  end
end
