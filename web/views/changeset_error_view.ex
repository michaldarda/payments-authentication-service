defmodule AuthenticationService.ChangesetErrorView do
  use AuthenticationService.Web, :view

  def render("error.json", %{changeset: changeset}) do
    %{errors: changeset}
  end
end
