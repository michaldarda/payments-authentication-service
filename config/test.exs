use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :authentication_service, AuthenticationService.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :authentication_service, AuthenticationService.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool: Ecto.Adapters.SQL.Sandbox,
  username: "ecto",
  password: "ecto",
  database: "payments_test",
  size: 1 # Use a single connection for transactional tests
