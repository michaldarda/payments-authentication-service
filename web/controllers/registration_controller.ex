defmodule AuthenticationService.RegistrationController do
  use AuthenticationService.Web, :controller

  alias AuthenticationService.Registration
  alias AuthenticationService.Account
  alias AuthenticationService.ChangesetErrorView

  def create(conn, %{"user" => user_params}) do
    changeset = Registration.proceed(user_params)

    if changeset.valid? do
      user = Repo.insert!(changeset)

      Account.changeset(%Account{},
      %{user_id: user.id, balance: 0.0})
      |>  Repo.insert!

      conn
      |> render(user: user)
      # |> halt
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render(ChangesetErrorView, "error.json", changeset: %{})
      # |> halt
    end
  end
end
