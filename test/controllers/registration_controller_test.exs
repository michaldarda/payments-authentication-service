defmodule AuthenticationService.RegistrationControllerTest do
  use AuthenticationService.ConnCase

  test "creates new user" do
    user_params = %{
      "user" => %{
        email: "john@example.com",
        login: "johnexample",
        password: "johnexample",
        password_confirmation: "johnexample"
      }}

    conn =
    conn()
    |> put_req_header("accept", "application/json")
    |> put_req_header("content-type", "application/json")
    |> post "/registration", user_params

    assert json_response(conn, 200)
  end
end
