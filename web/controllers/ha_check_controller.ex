defmodule AuthenticationService.HaCheckController do
  use AuthenticationService.Web, :controller

  def show(conn, _params) do
    conn |> text "OK"
  end
end
