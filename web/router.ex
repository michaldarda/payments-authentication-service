defmodule AuthenticationService.Router do
  use AuthenticationService.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AuthenticationService do
    pipe_through :api

    get "/ha_check", HaCheckController, :show

    post "/authenticate", AuthenticationController, :create
    post "/sessions", SessionController, :create
    post "/registration", RegistrationController, :create
  end
end
