defmodule AuthenticationService.User do
  use AuthenticationService.Web, :model

  schema "users" do
    field :login, :string
    field :email, :string
    field :crypted_password, :string
    field :token, :string

    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    has_one :account, AuthenticationService.Account

    timestamps
  end

  @required_fields ~w(login email password password_confirmation token crypted_password)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If `params` are nil, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
  end
end
