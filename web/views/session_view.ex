defmodule AuthenticationService.SessionView do
  use AuthenticationService.Web, :view

  def render("create.json", %{session: session}) do
    %{
      "email" => session.email,
      "token" => session.token
    }
  end
end
