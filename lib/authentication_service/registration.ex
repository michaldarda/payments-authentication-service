defmodule AuthenticationService.Registration do
  import Ecto.Query
  import Ecto.Changeset, only: [put_change: 3]
  import Comeonin.Bcrypt, only: [hashpwsalt: 1]

  alias AuthenticationService.User

  def proceed(user_params) do
    token =
    Comeonin.Tools.random_bytes(25)
    |> :binary.bin_to_list
    |> Comeonin.BcryptBase64.encode
    |> to_string

    encrypted_password = hashpwsalt(user_params["password"])

    query = from u in AuthenticationService.User,
    where: u.token == ^token,
    limit: 1

    user_with_that_token = AuthenticationService.Repo.all(query)

    unless user_with_that_token, do: proceed(user_params)

    User.changeset(%User{}, %{
      "login" => user_params["login"],
      "email" => user_params["email"],
      "password" => user_params["password"],
      "password_confirmation" => user_params["password_confirmation"],
      "crypted_password" => encrypted_password,
      "token" => token
    })
  end
end
