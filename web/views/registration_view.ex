defmodule AuthenticationService.RegistrationView do
  use AuthenticationService.Web, :view

  def render("create.json", %{user: user}) do
    %{
      "email" => user.email,
      "token" => user.token
    }
  end
end
