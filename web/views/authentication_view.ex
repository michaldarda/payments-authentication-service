defmodule AuthenticationService.AuthenticationView do
  use AuthenticationService.Web, :view

  def render("create.json", %{current_user: user}) do
    %{
      "id"    => user.id,
      "email" => user.email,
      "token" => user.token
    }
  end
end
