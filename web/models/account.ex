defmodule AuthenticationService.Account do
  use AuthenticationService.Web, :model

  schema "accounts" do
    belongs_to :user, AuthenticationService.User, foreign_key: :user_id

    field :balance, :float, default: 0.0
    field :token, :string

    timestamps
  end

  @required_fields ~w(user_id)
  @optional_fields ~w(balance token)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If `params` are nil, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
