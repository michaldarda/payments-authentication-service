defmodule AuthenticationService.AuthenticateControllerTest do
  use AuthenticationService.ConnCase

  alias AuthenticationService.User

  test "creates new user" do
    user = %User{
      email: "john@example.com",
      login: "johnexample",
      password: "johnexample",
      password_confirmation: "johnexample",
      crypted_password: "johnexamplef",
      token: "mytoken"}

    user = user |> Repo.insert!

    conn =
    conn()
    |> put_req_header("accept", "application/json")
    |> put_req_header("content-type", "application/json")
    |> put_req_header("authorization", "Bearer mytoken")
    |> post "/authenticate"

    resp = json_response(conn, 200)
    assert %{"email" => _, "token" => _, "id" => _} = resp
  end
end
