use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
config :authentication_service, AuthenticationService.Endpoint,
  secret_key_base: "9tfJ6r/tUVdkoJd5uPQX+U9M/AJjoUHeIfIDy67T4qY11AUygCp8acnnk++HB3ME"

# Configure your database
config :authentication_service, AuthenticationService.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: "ecto://postgres:postgres@payments_db/payments_prod",
  size: 20 # The amount of database connections in the pool
