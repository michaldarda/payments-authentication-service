defmodule AuthenticationService.AuthenticationController do
  use AuthenticationService.Web, :controller

  alias AuthenticationService.Registration
  alias AuthenticationService.User
  alias AuthenticationService.ChangesetErrorView

  def create(conn, _params) do
    token = Plug.Conn.get_req_header(conn, "authorization")

    IO.inspect token

    authenticate(conn, token)
  end

  defp authenticate(conn, ["Bearer " <> token]) do
    user = Repo.one(from u in User,
    where: u.token == ^token)

    IO.inspect user

    unless user do
      unauthorized(conn)
    else
      assign(conn, :current_user, user)

      conn
      |> render(current_user: user)
      # |> halt
    end
  end

  defp authenticate(conn, _token) do
    unauthorized(conn)
  end

  defp unauthorized(conn) do
    conn
    |> put_status(:unauthorized)
    |> render(ChangesetErrorView, "error.json", changeset: %{"message"=> "you are not allowed!"})
    # |> halt
  end
end
